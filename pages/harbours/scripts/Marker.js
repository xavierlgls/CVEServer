class Marker {
    constructor(lat, lng, title) {
        this.lat = lat;
        this.lng = lng;
        this.marker = L.marker([lat, lng], { title: title });
    }

    display(map) {
        this.marker.addTo(map);
    }

    hide(map) {
        map.removeLayer(this.marker);
    }
}