class CurrentHarbour extends Harbour {
    constructor(lat, lng, name) {
        super(lat, lng, name);
        this.marker.setIcon(current_harbour_icon);
    }
}