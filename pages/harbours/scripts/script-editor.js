var map;
var currentHarbour;
var storedHarbours = [];
var storedHarbourNumber;
var clusterGroup;
var selectedCount;

var harboursBeingSent = {
    batchSize: 500
};


var current_harbour_icon = L.icon({
    iconUrl: "../../assets/icon_current_harbour.png",
    iconAnchor: [15, 30]
});
var selected_harbour_icon = L.icon({
    iconUrl: "../../assets/icon_selected_harbour.png",
    iconAnchor: [15, 40]
});
var stored_harbour_icon = L.icon({
    iconUrl: "../../assets/icon_stored_harbour.png",
    iconAnchor: [15, 30]
});

function csvImporterSetup() {
    var fileInput = document.getElementById("file-input");
    var fileReader = new FileReader();

    fileReader.addEventListener("loadstart", function () {
        var fileName = fileInput.files[0].name;
        var names = fileName.split(".");
        var name = names.splice(0, names.length - 1).join(".");
        document.getElementById("file-name-display").value = "importing " + name + "...";
    });

    fileReader.addEventListener("load", function () {
        try {
            var list = [];
            var fileContent = $.csv.toArrays(fileReader.result);
            fileContent.forEach(row => {
                if (row[0] != "Latitude") {
                    var obj = {};
                    obj.lat = row[0];
                    obj.lng = row[1];
                    obj.name = row[2];
                    list.push(obj);
                }
            });
            sendHarbours_init(list, "csv");
        } catch (error) {
            alert(error);
        }
    });

    fileInput.addEventListener("change", function () {
        fileReader.readAsText(this.files[0]);
    });
}


$().ready(init);

function init() {
    selectedCount = 0;

    map = L.map('map', { doubleClickZoom: false }).setView([0, 0], 2);
    map.on("dblclick", onMapDbleClick);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', { foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' }).addTo(map);

    $.post("../../backend/harbours.php", {
        request: "get",
        "app-token": getAppToken()
    }, function (result, status) {
        if (status == "success") {
            displayStoredHarbours(result);
            storedHarbourNumber = storedHarbours.length;
        } else if (status == "timeout" || status == "error") {
            console.log("error");
        }
    });

    $('#add-form').submit(function (event) {
        var harbour = [{
            name: $('#add-form input[name="name"]').val(),
            lat: $('#add-form input[name="lat"]').val(),
            lng: $('#add-form input[name="lng"]').val()
        }];
        sendHarbours_init(harbour, "form");
        event.preventDefault();
    });


    $('#remove').click(function () {
        var confirmString = "Do you really want to remove";
        var ids = [];
        storedHarbours.filter(harbour => harbour.selected).forEach(harbour => {
            confirmString += ("\n - " + harbour.name);
            ids.push(harbour.id);
        });
        if (ids.length == 0) {
            alert("Please select at least one harbour !");
        } else {
            if (confirm(confirmString)) {
                $.post("../../backend/harbours.php", {
                    request: "remove",
                    "app-token": getAppToken(),
                    "id-list": JSON.stringify(ids)
                }, function (result, status) {
                    if (status == "success") {
                        //TODO: selected harbours number
                        var toRemove = storedHarbours.filter(harbour => harbour.selected);
                        toRemove.forEach(harbour => {
                            harbour.selected = false;
                            harbour.hide(map);
                            clusterGroup.removeLayer(harbour.marker);
                            //TODO: remove from list ?
                        });
                        storedHarbourNumber -= toRemove.length;
                        updateStoredNumber();
                    } else if (status == "timeout" || status == "error") {
                        console.log("error");
                    }
                });
            }

        }

    });

    csvImporterSetup();

}

function onMapDbleClick(e) {
    if (currentHarbour)
        currentHarbour.hide(map);
    currentHarbour = new CurrentHarbour(e.latlng.lat, e.latlng.lng, "");
    currentHarbour.display(map);
    $('input[name="lat"]').val(Math.round(1000 * e.latlng.lat) / 1000);
    $('input[name="lng"]').val(Math.round(1000 * e.latlng.lng) / 1000);
}

function displayStoredHarbours(json) {

    if (clusterGroup)
        map.removeLayer(clusterGroup);
    storedHarbours = [];
    clusterGroup = L.markerClusterGroup();
    for (var i = 0; i < json["harbours"].length; i++) {
        var jsonHarbour = json["harbours"][i];
        var lat = parseFloat(jsonHarbour["lat"]);
        var lng = parseFloat(jsonHarbour["lng"]);
        var id = parseInt(jsonHarbour["harbour_id"]);
        var name = jsonHarbour["name"];

        var harbour = new StoredHarbour(lat, lng, name, id);
        storedHarbours.push(harbour);
        clusterGroup.addLayer(harbour.marker);
    }
    map.addLayer(clusterGroup);
}

function sendHarbours_init(list, sender) {
    harboursBeingSent.totalSize = list.length;
    harboursBeingSent.batchsSent = -1;
    harboursBeingSent.dataToSend = list;
    harboursBeingSent.sender = sender;
    this.sendHarbours_onSendSuccess(null, "init");
}

function sendHarbours_sendBatch(batch) {
    console.log("<sendHarbours_sendBatch>");
    console.log("batch length: " + batch.length);
    $.post("../../backend/harbours.php", {
        request: "add",
        "app-token": getAppToken(),
        "user-id": $('#add-form input[name="user_id"]').val(),
        "harbours": JSON.stringify(batch)
    }, sendHarbours_onSendSuccess
    );
}

function sendHarbours_onSendSuccess(result, status) {
    console.log("<sendHarbours_onSendSuccess>");
    if (status == "success" || status == "init") {
        harboursBeingSent.batchsSent++;
        if (harboursBeingSent.batchsSent > 0) {
            console.log(result)
            var ids = result.ids;
            storedHarbourNumber += ids.length;
            updateStoredNumber();
            harboursBeingSent.lastBatch.forEach(point => {
                var newHarbour = new StoredHarbour(parseFloat(point.lat), parseFloat(point.lng), point.name, parseInt(ids.shift()));
                storedHarbours.push(newHarbour);
                clusterGroup.addLayer(newHarbour.marker);
            });
        }
        if (harboursBeingSent.dataToSend.length == 0) {
            switch (harboursBeingSent.sender) {
                case "csv":
                    displayProgress(1);
                    document.getElementById("file-name-display").value = "import done";
                    setTimeout(function () {
                        document.getElementById("file-name-display").value = "";
                    }, 3000);
                    break;
                case "form":
                    currentHarbour.hide(map);
                    $('#add-form')[0].reset();
                    break;
            }
        } else {
            displayProgress(harboursBeingSent.batchsSent * harboursBeingSent.batchSize / harboursBeingSent.totalSize)
            //little hack for UI update
            setTimeout(function () {
                var batch = harboursBeingSent.dataToSend.splice(0, (harboursBeingSent.dataToSend.length > harboursBeingSent.batchSize ? harboursBeingSent.batchSize : harboursBeingSent.dataToSend.length));
                harboursBeingSent.lastBatch = batch;
                sendHarbours_sendBatch(batch);
            }, 0);
        }
    }
}

function updateStoredNumber() {
    $('#harbour-nbre').text(storedHarbourNumber);
}

function displayProgress(progress) {
    var container = document.querySelector("#send-progress");
    var progressBar = document.querySelector("#send-progress .progress-bar");
    if (progress < 0) {
        hide(container);
    } else {
        if (progress > 1) {
            progress = 1;
        }
        reveal(container);
        progressBar.style.width = Math.round(100 * progress) + "%";
        progressBar.innerHTML = Math.round(100 * progress) + "%";
        if (progress >= 1) {
            setTimeout(function () {
                hide(container);
            }, 3000);
        }
    }
}

function hide(elt) {
    elt.classList.add("hidden");
}

function reveal(elt) {
    elt.classList.remove("hidden");
}

//TODO: use
// function getMarkersInside(markers, polygon) {
//     var pts = markers.toGeoJSON()
//     return turf.within(pts, polygon.toGeoJSON());
// }