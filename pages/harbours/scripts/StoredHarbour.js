class StoredHarbour extends Harbour {
    constructor(lat, lng, name, id, noSelection) {
        super(lat, lng, name);
        this._noSelection = !(noSelection === undefined) && noSelection == true;
        this._selected = false;
        this._id = id;
        this._name = name;
        this.marker.setIcon(stored_harbour_icon);
        this.marker.on("click", this.onClick.bind(null, this));
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name;
    }

    get selected() {
        return this._selected;
    }

    set selected(value) {
        if (value && !this._selected) {
            clusterGroup.removeLayer(this.marker);
            map.addLayer(this.marker);
            this._selected = true;
            selectedCount++;
        } else if (this._selected) {
            clusterGroup.addLayer(this.marker);
            map.removeLayer(this.marker);
            this._selected = false;
            selectedCount--;
        }
    }

    onClick(parent) {
        parent.select(!parent.selected, parent);
    }

    select(bool, parent) {
        if (this._noSelection) {
            return;
        }
        var parentElt = this;
        if (parent) {
            parentElt = parent;
        }

        parentElt.selected = bool;
        if (parentElt.selected) {
            parentElt.marker.setIcon(selected_harbour_icon);
        } else {
            parentElt.marker.setIcon(stored_harbour_icon);
        }
    }

}