var map;
var storedHarbours = [];
var clusterGroup;

var stored_harbour_icon = L.icon({
    iconUrl: "../../assets/icon_stored_harbour.png",
    iconAnchor: [15, 30]
});


$().ready(init);

function init() {

    map = L.map('map', { doubleClickZoom: false }).setView([0, 0], 2);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', { foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' }).addTo(map);

    $.post("../../backend/harbours.php", {
        request: "get",
        "app-token": getAppToken()
    }, function (result, status) {
        if (status == "success") {
            displayStoredHarbours(result);
        } else if (status == "timeout" || status == "error") {
            console.log("error");
        }
    });

}

function displayStoredHarbours(json) {
    if (clusterGroup)
        map.removeLayer(clusterGroup);
    storedHarbours = [];
    clusterGroup = L.markerClusterGroup();
    for (var i = 0; i < json["harbours"].length; i++) {
        var jsonHarbour = json["harbours"][i];
        var lat = parseFloat(jsonHarbour["lat"]);
        var lng = parseFloat(jsonHarbour["lng"]);
        var id = parseInt(jsonHarbour["harbour_id"]);
        var name = jsonHarbour["name"];

        var harbour = new StoredHarbour(lat, lng, name, id);
        storedHarbours.push(harbour);
        clusterGroup.addLayer(harbour.marker);
    }
    map.addLayer(clusterGroup);
}

//TODO: use
// function getMarkersInside(markers, polygon) {
//     var pts = markers.toGeoJSON()
//     return turf.within(pts, polygon.toGeoJSON());
// }