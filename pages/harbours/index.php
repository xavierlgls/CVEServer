<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<!-- leaflet lib -->
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
<!-- marker cluster -->
<link rel="stylesheet" href="styles/MarkerCluster.css" />
<link rel="stylesheet" href="styles/MarkerCluster.Default.css" />
<script src="scripts/leaflet.markercluster-src.js"></script>
<!-- local script -->
<script src="scripts/Marker.js"></script>
<script src="scripts/Harbour.js"></script>
<script src="scripts/CurrentHarbour.js"></script>
<script src="scripts/StoredHarbour.js"></script>
<?php if ($_SESSION["auth"]->permission >= 1) { ?>
    <script src="scripts/jquery.csv.min.js"></script>
    <script src="scripts/script-editor.js"></script>
<?php } else { ?>
    <script src="scripts/script.js"></script>
<?php } ?>
<style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    #map {
        height: 100%;
        margin: 1em;
    }

    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>
<link rel="stylesheet" href="styles/style.css">
<input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Harbours</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<?php if ($_SESSION["auth"]->permission >= 1) { ?>
    <!-- if the user can edit the harbour database -->
    <p>This tool manages the database where all harbours are stored. This harbours are locations where players will be able to make deals. <strong><span id="harbour-nbre"><?= $db->getHarboursNumber() ?></span> harbours</strong> are currently stored !</p>
    <div class="row content-wrapper">
        <!-- MAP -->
        <div class="col-12 col-sm-9">
            <div id="map" style="height: 650px;"></div>
            <div id="select-count" class="hidden">
                <button type="button" class="btn btn-success disable">Selected harbours <span class="badge">0</span></button>
                <button type="button" class="btn btn-danger" onclick="unselectAllHarbours();">Unselect all harbours</button>
            </div>
        </div>
        <div class="col-12 col-sm-3">
            <h3>Add a new harbour</h3>
            <form id="add-form" action="../../backend/harbours.php" method="post" autocomplete="off">
                <input type="hidden" name="user_id" value="<?= $_SESSION["auth"]->id ?>">
                <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                <div class="form-group">
                    <label for="lat">Latitude</label>
                    <input type="number" class="form-control" name="lat" step="0.001" placeholder="double click the map to fill in" required>
                </div>
                <label for="lng">Longitude</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="lng" step="0.001" placeholder="double click the map to fill in" required>
                </div>
                <label for="name">Harbour name</label>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="be concise" required>
                </div>
                <button type="submit" class="btn btn-primary">Add this harbour</button>
            </form>
            <h3>Remove harbours</h3>
            <button id="remove" class="btn btn-danger">Remove all selected harbours</button>
            <h3>Import harbours</h3>
            <div class="custom-file">
                <div class="input-group">
                    <label class="input-group-btn">
                        <span class="btn btn-primary">
                            Browse&hellip; <input type="file" id="file-input" style="display: none;">
                        </span>
                    </label>
                    <input type="text" id="file-name-display" class="form-control" readonly>
                </div>
                <p class="help-block">the file must be a <strong>.csv</strong> file whose structure is lat;lng;name</p>
            </div>
            <div id="send-progress" class="progress hidden">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    0%
                </div>
            </div>
            <h3>Export stored harbours</h3>
            <a href="../../downloads/harbours.csv" download>
                <button class="btn btn-primary">Download</button>
            </a>
            <p class="help-block">generates a .csv file whose structure is lat;lng;name</p>
        </div>
    </div>
<?php } else if ($_SESSION["auth"]) { ?>
    <!-- if the user cannot edit the harbour database -->
    <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
    <div class="container content-wrapper">
        <div class="row">
            <div class="col-12">
                <div id="map" style="height: 650px;"></div>
            </div>
        </div>
    </div>
<?php } ?>
<?php require("../components/footer.php"); ?>