<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");

function displayABoat($boat)
{
    $str = '';
    $str .= '<tr>';
    $str .= ('<td>' . $boat->name . '</td>');
    // food capacity
    $str .= '<td><input type="number" class="form-control" style="width: 150px;" initial_value="' . $boat->food_capacity . '" value="' . $boat->food_capacity . '" name="food_capacity_' . $boat->type_id . '"></td>';
    //max speed
    $str .= '<td><input type="number" class="form-control" style="width: 150px;" initial_value="' . $boat->max_speed . '" value="' . $boat->max_speed . '" name="max_speed_' . $boat->type_id . '"></td>';
    $str .= '</tr>';
    echo $str;
}

?>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Boat characteristics management</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container">
    <?php if (isset($_SESSION['auth'])) {
        if ($_SESSION['auth']->permission >= 2) { ?>
            <form action="../../backend/boat_characteristics.php" method="post">
                <input type="hidden" name="app-token" value="<?php echo $CVE_WEB_API_KEY; ?>">
                <table class="table table-hover">
                    <tr>
                        <th>
                            Type
                        </th>
                        <th>
                            Food capacity (days)
                        </th>
                        <th>
                            Max speed (knots)
                        </th>
                    </tr>
                    <?php
                    $boats = $db->getSailawayBoatCharacteristics();
                    foreach ($boats as $boat) {
                        displayABoat($boat);
                    }
                    ?>
                </table>
                <button type="submit" class="btn btn-warning">Update characteristics</button>
            </form>
    <?php }
    } else {
        addFlashError('you dont have the permission to access this page');
        header('Location: ../home');
    } ?>
</div>

<?php require("../components/footer.php"); ?>