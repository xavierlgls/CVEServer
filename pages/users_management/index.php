<?php

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Users management</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container content-wrapper">
    <?php if (isset($_SESSION['auth'])) {
        if ($_SESSION['auth']->permission >= 2) { ?>
            <p>
                Type something in the input field to search the table for username or emails:
            </p>
            <input class="form-control" id="search-input" type="text" placeholder="Search..">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Registration date</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="users-body">
                    <tr>
                        <td>
                            <h4>Administrators</h4>
                        </td>
                    </tr>
                    <?php
                    $users = $db->getAllAdministrators();
                    foreach ($users as $user) {
                        $dateStr = '-';
                        if ($user->confirmed_at != NULL) {
                            $dateStr = substr($user->confirmed_at, 0, 10);
                        }
                    ?>
                        <tr>
                            <td><?= $user->email ?></td>
                            <td><?= $db->getSailawayUserName($user->sw_user_id) ?></td>
                            <td><?= $dateStr ?></td>
                            <td></td>
                            <td>
                                <form class="delete-form" action="../../backend/users_management.php" method="post">
                                    <input type="hidden" name="action" value="deletion" />
                                    <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                                    <input type="hidden" name="id" value="<?= $user->id ?>" />
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <td>
                            <h4>Contributors</h4>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $users = $db->getAllContributors();
                    foreach ($users as $user) {
                        $dateStr = '-';
                        if ($user->confirmed_at != NULL) {
                            $dateStr = substr($user->confirmed_at, 0, 10);
                        }
                    ?>
                        <tr>
                            <td><?= $user->email ?></td>
                            <td><?= $db->getSailawayUserName($user->sw_user_id) ?></td>
                            <td><?= $dateStr ?></td>
                            <td>
                                <form class="reset-contributor-form" action="../../backend/users_management.php" method="post">
                                    <input type="hidden" name="action" value="permission" />
                                    <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                                    <input type="hidden" name="new-permission" value="0" />
                                    <input type="hidden" name="id" value="<?= $user->id ?>" />
                                    <button type="submit" class="btn btn-warning">Remove permission</button>
                                </form>
                            </td>
                            <td>
                                <form class="delete-form" action="../../backend/users_management.php" method="post">
                                    <input type="hidden" name="action" value="deletion" />
                                    <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                                    <input type="hidden" name="id" value="<?= $user->id ?>" />
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <td>
                            <h4>Players</h4>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $users = $db->getAllPlayers();
                    foreach ($users as $user) {
                        $dateStr = '-';
                        if ($user->confirmed_at != NULL) {
                            $dateStr = substr($user->confirmed_at, 0, 10);
                        }
                    ?>
                        <tr <?php if ($user->confirmed_at == NULL) {
                                echo 'class="table-warning"';
                            } ?>>
                            <td><?= $user->email ?></td>
                            <td><?= $db->getSailawayUserName($user->sw_user_id) ?></td>
                            <td><?= $dateStr ?></td>
                            <td>
                                <?php if ($user->confirmed_at != NULL) { ?>
                                    <form class="set-contributor-form" action="../../backend/users_management.php" method="post">
                                        <input type="hidden" name="action" value="permission" />
                                        <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                                        <input type="hidden" name="new-permission" value="1" />
                                        <input type="hidden" name="id" value="<?= $user->id ?>" />
                                        <button type="submit" class="btn btn-primary">Set contributor</button>
                                    </form>
                                <?php } else { ?>
                                    <form class="confirm-form" action="../../backend/users_management.php" method="post">
                                        <input type="hidden" name="action" value="registration-confirmation" />
                                        <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                                        <input type="hidden" name="id" value="<?= $user->id ?>" />
                                        <button type="submit" class="btn btn-success">Confirm registration</button>
                                    </form>
                                <?php } ?>
                            </td>
                            <td>
                                <form class="delete-form" action="../../backend/users_management.php" method="post">
                                    <input type="hidden" name="action" value="deletion" />
                                    <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                                    <input type="hidden" name="id" value="<?= $user->id ?>" />
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
    <?php }
    } else {
        addFlashError('you dont have the permission to access this page');
        header('Location: ../home');
    } ?>
</div>

<script>
    $(document).ready(function() {
        $("#search-input").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#users-body tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        $(".reset-contributor-form").submit(function(event) {
            if (!confirm('Do you want to remove contributor permissions ?')) {
                event.preventDefault();
            }
        });
        $(".set-contributor-form").submit(function(event) {
            if (!confirm('Do you want to grant contributor permissions ?')) {
                event.preventDefault();
            }
        });
        $(".confirm-form").submit(function(event) {
            if (!confirm('Do you want to confirm the registration ?')) {
                event.preventDefault();
            }
        });
        $(".delete-form").submit(function(event) {
            if (!confirm('Do you want to delete this user account ?')) {
                event.preventDefault();
            }
        });
    });
</script>

<?php require("../components/footer.php"); ?>