<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require('../../../config_v1.php');
require('../../backend/db.php');
require('../../backend/tools.php');
$db = new DB();

if (isset($_COOKIE['remember']) and !isset($_SESSION['auth'])) {
    $remember_token = $_COOKIE['remember'];
    $parts = explode('==', $remember_token);
    $user_id = $parts[0];
    $user = $db->getUserFromId($user_id);
    if ($user != NULL) {
        $expected_token = $user->id . '==' . $user->remember_token/* . sha1($user->id, $REMEMBER_SALT_KEY)*/;
        if ($remember_token == $expected_token) {
            if ($user->permission >= 1) {
                addFlashError('The previous account had permission, the user has to manually sign in');
            } else {
                $_SESSION['auth'] = $user;
                addFlashSuccess('Welcome back <u>' . $user->username . '</u> ! You have been automatically logged in because you have checked "remember". If you don\'t want anymore, please click on logout while exiting.');
                setcookie('remember', $remember_token, time() + 60 * 60 * 24 * 7, "/");
            }
        } else {
            setcookie('remember', NULL, -1, "/");
        }
    } else {
        setcookie('remember', NULL, -1, "/");
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Complete Voyaging Experience</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>

<body>
    <script>
        // Returns the current app token
        function getAppToken() {
            return "<?= $CVE_WEB_API_KEY ?>";
        }
    </script>