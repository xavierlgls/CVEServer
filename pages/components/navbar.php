<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <a class="navbar-brand" href="../credits">CVE</a>

    <?php
    if (isset($_SESSION['auth'])) {
        if ($_SESSION['auth']->permission  == 2) {
            echo '<a class="navbar-brand text-danger">Administrator</a>';
        } else if ($_SESSION['auth']->permission == 1) {
            echo '<a class="navbar-brand text-danger">Contributor</a>';
        }
        echo '<a class="navbar-brand text-warning">' . $db->getSailawayUserName($_SESSION['auth']->sw_user_id) . '</a>';
        echo '<span id="time-update" style="color: white; transition: 1s;"></span>';
    }
    ?>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse d-flex flex-row-reverse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="../home">Home</a>
            </li>
            <?php if (isset($_SESSION['auth'])) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="../harbours">Harbours</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../dashboard">Dashboard</a>
                </li>
                <?php if ($_SESSION['auth']->permission >= 2) { ?>
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin actions</a> <!-- lien -->
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="../users_management">Users</a>
                                <a class="dropdown-item" href="../boat_characteristics_management">Boat characteristics</a>
                                <a class="dropdown-item" href="../sw_api_access_management">Sailaway API access</a>
                                <a class="dropdown-item" href="../logs">Logs</a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <li class="nav-item">
                    <div class="dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">My account</a> <!-- lien -->
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="../my_account">Settings</a>
                            <a class="dropdown-item" onclick="logOut();" style="cursor: pointer;">Log out</a>
                        </div>
                    </div>
                </li>
            <?php } else { ?>
                <li class="nav-item">
                    <a class="nav-link" href="../login">Log in</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../register">Sign up</a>
                </li>
            <?php } ?>
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Help</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="../help_en"><img src="../../assets/english_flag.png" alt="english" height="15"></a>
                        <a class="dropdown-item" href="../help_fr"><img src="../../assets/french_flag.png" alt="français" height="15"></a>
                        <a class="dropdown-item" href="../help_ned"><img src="../../assets/dutch_flag.png" alt="frans" height="15"></a>
                        <a class="dropdown-item" href="../help_es"><img src="../../assets/spanish_flag.png" alt="español" height="15"></a>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</nav>

<script>
    <?php if (isset($_SESSION['auth'])) {
        // get time left before next update
        $now = time();
        $configJson = json_decode(file_get_contents('../../backend/cron-jobs/cron-settings.json'));
        $lastQueryDate = intval($configJson->last_query_date);
        $rate = intval($configJson->rate);
        $targetPeriodMin = intval((24 * 60) / $rate);
        $timeElapsedSinceLastQueryMs =  intval(($now - $lastQueryDate) / 60);
        $timeLeftMin = round($targetPeriodMin - $timeElapsedSinceLastQueryMs);
        echo 'var timeLeft =  ' . $timeLeftMin . ';';
    ?>
        var timerId;
        let blinkTimer = 0;

        function displayTimeLeft() {
            if (timeLeft >= 0) {
                $('#time-update').text((timeLeft + 1) + " min before next synchronization");
            } else {
                clearInterval(timerId);
                $('#time-update').text("refresh the page to update your data");
                blink();
            }
        }

        function blink() {
            if (blinkTimer % 2 == 0) {
                $('#time-update').css("color", "red");
            } else {
                $('#time-update').css("color", "white");
            }
            blinkTimer++;
            setTimeout(blink, 2500);
        }

        $(document).ready(function() {
            displayTimeLeft();
            timerId = setInterval(function() {
                timeLeft--;
                displayTimeLeft();
            }, 60 * 1000);
        });
    <?php } ?>

    function logOut() {
        $.post("../../backend/authentication.php", {
                action: "logout",
                "app-token": "<?= $CVE_WEB_API_KEY ?>"
            },
            function(result, status) {
                if (status == "success") {
                    window.location.href = "../home";
                }
            }
        );
    }
</script>