<?php

if (isset($_SESSION['flash-error'])) {
    foreach ($_SESSION['flash-error'] as $error) {
?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?= $error ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php
    }
    unset($_SESSION['flash-error']);
}
if (isset($_SESSION['flash-success'])) {
    foreach ($_SESSION['flash-success'] as $success) {
    ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?= $success ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
<?php
    }
    unset($_SESSION['flash-success']);
}
