$().ready(init);

var thresholds = {
    nominal: 50,
    excessive: 100
}

function init() {
    $.post("../../backend/rest/get-sw-api-uses.php", {
        "app-token": getAppToken()
    }, function (result, status) {
        if (status == "success") {
            initGraph(result);
        } else if (status == "timeout" || status == "error") {
            console.log("error");
        }
    });

    $("#activator-button").click(function () {
        if ($(this).hasClass("btn-danger")) {
            requestEnableService();
        } else if ($(this).hasClass("btn-success")) {
            requestDisableService();
        }
    });

    $('input[name="rate"').on("input", function () {
        updateRatecaption($(this).val());
    });
    updateRatecaption($('input[name="rate"').val());

    $("#rate-submit").click(function () {
        var newRate = $('input[name="rate"').val();
        if (newRate > 0 && newRate <= 288) {
            $("#rate-submit").removeClass("btn-primary");
            $("#rate-submit").addClass("btn-warning");
            $("#rate-submit").text("updating");
            $.post("../../backend/rest/control-sw-api-access.php", {
                "app-token": getAppToken(),
                "action": "set-rate",
                "rate": newRate
            }, function (result, status) {
                if (status == "success" && result.success) {
                    onRateSet(result);
                } else {
                    console.log("error");
                    $("#rate-submit").removeClass("btn-warning");
                    $("#rate-submit").addClass("btn-primary");
                    $("#rate-submit").text("update the rate");
                }
            });
        }
    });
}

function initGraph(result) {
    var data = [];
    Object.keys(result.history).forEach(day => {
        var obj = {};
        obj.x = new Date(day);
        obj.y = result.history[day];
        data.push(obj);
    });
    var colors = [];
    $.each(data, function (index, value) {
        if (value.y < thresholds.nominal) {
            colors[index] = '#bcf60c';
        } else if (value.y < thresholds.excessive) {
            colors[index] = '#f58231';
        } else {
            colors[index] = '#e6194b';
        }
    });
    var ctx = document.getElementById('chart').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [
                {
                    label: 'number of use per day',
                    data: data,
                    backgroundColor: colors
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    offset: true
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    title: function (tooltipItem) {
                        var date = new Date(Date.parse(tooltipItem[0].label));
                        return date.toDateString();
                    }
                }
            }
        }
    });
}

function updateRatecaption(rateDay) {
    if (rateDay != "") {
        var rateStr = "";
        if (rateDay == 1) {
            rateStr = "request";
        } else {
            rateStr = "requests";
        }
        var periodStr = "";
        var periodMin = Math.floor((24 * 60) / rateDay);
        if (periodMin < 60) {
            periodStr = periodMin + " min";
        } else if (periodMin % 60 == 0) {
            periodStr = Math.floor(periodMin / 60) + "h";
        } else {
            periodStr = Math.floor(periodMin / 60) + "h" + (periodMin % 60) + "min";
        }

        var text = `${rateStr} per day (one request every ${periodStr})`;

        $("#rate-caption").text(text);
    }
}

// Service enabler

function requestEnableService() {
    $.post("../../backend/rest/control-sw-api-access.php", {
        "app-token": getAppToken(),
        "action": "enable-service"
    }, function (result, status) {
        if (status == "success") {
            onServiceEnabled();
        } else if (status == "timeout" || status == "error") {
            console.log("error");
        }
    });
    $("#activator-button").removeClass("btn-danger");
    $("#activator-button").addClass("btn-warning");
    $("#activator-button").text("enabling...");
}

function onServiceDisabled() {
    $("#activator-button").removeClass("btn-warning");
    $("#activator-button").addClass("btn-danger");
    $("#activator-button").text("disabled");
}

function requestDisableService() {
    $.post("../../backend/rest/control-sw-api-access.php", {
        "app-token": getAppToken(),
        "action": "disable-service"
    }, function (result, status) {
        if (status == "success") {
            onServiceDisabled();
        } else if (status == "timeout" || status == "error") {
            console.log("error");
        }
    });
    $("#activator-button").removeClass("btn-success");
    $("#activator-button").addClass("btn-warning");
    $("#activator-button").text("disabling...");
}

function onServiceEnabled() {
    $("#activator-button").removeClass("btn-warning");
    $("#activator-button").addClass("btn-success");
    $("#activator-button").text("enabled");
}

// Rate setting

function onRateSet(result) {
    //update the button
    $("#rate-submit").removeClass("btn-warning");
    $("#rate-submit").addClass("btn-primary");
    $("#rate-submit").text("update the rate");
    //update the diplayed current rate
    $("#current-rate").text(result.rate + " request" + (result.rate > 1 ? "s" : ""));
}