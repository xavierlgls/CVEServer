<?php

function isServiceEnabled()
{
    $json = json_decode(file_get_contents('../../backend/cron-jobs/cron-settings.json'));
    return $json->enabled;
}

function getServiceRate()
{
    $json = json_decode(file_get_contents('../../backend/cron-jobs/cron-settings.json'));
    return $json->rate;
}

function getRateCaption()
{
    $rate = getServiceRate();
    if ($rate > 1) {
        return $rate . " requests";
    } else {
        return "1 request";
    }
}

function drawStateButton()
{
    $serviceRunning = isServiceEnabled();
    echo '<button id="activator-button" type="button" class="btn btn-' . ($serviceRunning ? 'success' : 'danger') . '">' . ($serviceRunning ? 'enabled' : 'disabled') . '</button>';
}

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="script.js"></script>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Sailaway API access management</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
<div class="container content-wrapper">
    <?php if (isset($_SESSION['auth'])) {
        if ($_SESSION['auth']->permission >= 2) { ?>
            <h3>History of use (last month)</h3>
            <canvas id="chart"></canvas>
            <hr>
            <h3>Service state <?php drawStateButton(); ?></h3>
            <h3>Service rate</h3>
            <p>
                <strong>Current rate: </strong><span id="current-rate"><?= getRateCaption() ?></span> per day
            </p>
            <input type="number" name="rate" max="288" min="1" class="form-control" style="display: inline-block; width: 80px;" value="<?= getServiceRate() ?>">
            <span id="rate-caption"></span>
            <button id="rate-submit" type="button" class="btn btn-primary">update the rate</button>
            <hr>
            <a href="../../backend/cron-jobs/routine.php">
                <button id="manual-update" type="button" class="btn btn-warning">manual update of sailaway data</button>
            </a>
            <span class="text-danger">do not overdo the use of this feature (for test purpose only)</span>
    <?php }
    } else {
        addFlashError('you dont have the permission to access this page');
        header('Location: ../home');
    } ?>
</div>

<?php require("../components/footer.php"); ?>