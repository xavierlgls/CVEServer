<?php
require('../components/header.php');
$getOk = (isset($_GET['user_id']) and isset($_GET['token']) and isset($_GET['username']));
if (!$getOk) {
    addFlashError('wrong request content');
}
require("../components/navbar.php");
require("../components/flash.php");

if ($getOk) {
?>
    <div style="padding: 0.5em; background-color: #EEEEEE;">
        <h3>Set <strong><?= $_GET['username'] ?></strong> new password</h3>
    </div>
    <hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
    <div class="container content-wrapper">
        <div class="row">
            <div class="col-md text-center"></div>
            <div class="col-md text-center">
                <form id="password-change" action="../../backend/authentication.php" method="post">
                    <input type="hidden" name="action" value="password-reset">
                    <input type="hidden" name="id" value="<?= $_GET['user_id'] ?>">
                    <input type="hidden" name="token" value="<?= $_GET['token'] ?>">
                    <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">&#128273;</span>
                            </div>
                            <input type="password" class="form-control" name="password-1" placeholder="password" style="text-align:center;" required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">&#128273;</span>
                            </div>
                            <input type="password" class="form-control" name="password-2" placeholder="password confirmation" style="text-align:center;" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Set new password</button>
                    </div>
                </form>
            </div>
            <div class="col-md text-center"></div>
        </div>
    </div>

    <script>
        function displayErrors(errors) {
            var navbar = document.querySelector("nav.navbar");
            errors.forEach(error => {
                var div = document.createElement("div");
                div.classList = ["alert alert-danger alert-dismissible fade show"];
                div.role = "alert";
                if (navbar != null) {
                    // display after navbar
                    navbar.parentNode.insertBefore(div, navbar.nextSibling);
                } else {
                    // display at the beginning of the body
                    document.body.prepend(div);
                }
                div.innerHTML = error + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            });
        }
        $("#password-change").submit(function(event) {
            var errors = [];
            if ($('#password-change input[name="password-1"]').val() != $('#password-change input[name="password-2"]').val()) {
                errors.push("passwords are different");
            } else if ($('#password-change input[name="password-1"]').val().length < 6) {
                errors.push("the password must contain almost 6 characters");
            }
            if (errors.length > 0) {
                event.preventDefault();
                displayErrors(errors);
            }
        });
    </script>
<?php
} else {
?>
    <div class="container content-wrapper"></div>
<?php
}
require('../components/footer.php');
?>