<?php
require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
$boat = $db->getCVEBoatFromCVEUserId($_SESSION["auth"]->id);
$food_capacity = $db->getCharacteristicsFromType($boat->type)->food_capacity;
$boat_charac = $db->getCharacteristicsFromType($boat->type);
$trajectory = $db->getTrajectory($boat->id);
function reducer($acc, $elt)
{
    if ($acc) {
        if ($acc->date < $elt->date) {
            return $elt;
        } else {
            return $acc;
        }
    } else {
        return $elt;
    }
}
if ($trajectory) {
    $lat = floatval(array_reduce($trajectory, "reducer")->lat);
    $lng = floatval(array_reduce($trajectory, "reducer")->lng);
    $nearestHarbours = $db->getNearestHarbours($lat, $lng, $HARBOUR_NBRE_IN_DASHBOARD);
    $nearestHarbour = $db->getNearestHarbour($lat, $lng);
    $inHarbour = $nearestHarbour["dist"] < $HARBOUR_RADIUS;
}
?>
<!-- leaflet lib -->
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
<!-- lib to rotate markers -->
<script src="leaflet.marker.rotate.js"></script>
<!-- local script -->
<?php if ($trajectory) { ?>
    <script>
        var boat_icon_dim = {
            height: 40,
            width: 40
        }

        var boat = {
            lat: <?= $lat ?>,
            lng: <?= $lng ?>,
            trajectory: <?php
                        echo '[';
                        foreach ($trajectory as $point) {
                            echo '{lat:' . $point->lat . ', lng: ' . $point->lng . '},';
                        }
                        echo ']';
                        ?>,
            angle: <?= floatval($boat->angle) ?>
        };

        var stored_harbour_icon = L.icon({
            iconUrl: "../../assets/icon_stored_harbour.png",
            iconAnchor: [15, 30]
        });
        var boat_icon = L.icon({
            iconUrl: "../../assets/icon_boat.png",
            iconSize: [boat_icon_dim.width, boat_icon_dim.height],
            iconAnchor: [boat_icon_dim.width / 2, boat_icon_dim.height / 2]
        });

        var harbours = [
            <?php
            foreach ($nearestHarbours as $harbour) {
                echo '{lat: ' . $harbour["harbour"]->lat . ', lng: ' . $harbour["harbour"]->lng . ', name: "' . $harbour["harbour"]->name . '", dist: ' .  $harbour["dist"] . '},';
            }
            ?>
        ];

        var harbour_radius = <?= $HARBOUR_RADIUS ?>; //nm

        $().ready(init);

        function init() {
            // map init
            map = L.map('map', {
                doubleClickZoom: false
            }).setView([boat.lat, boat.lng], 10);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
                foo: 'bar',
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
            }).addTo(map);
            var boatMarker = L.marker([boat.lat, boat.lng])
                .setIcon(boat_icon)
                .bindPopup('<h5 class="text-danger">my CVE boat</h5>')
                .addTo(map)
                .setRotationAngle(boat.angle)
                .setRotationOrigin("center");

            var harbourMarkers = [];

            harbours.forEach(harbour => {
                // circle
                L.circle([harbour.lat, harbour.lng], {
                    color: 'blue',
                    weight: 0,
                    fillColor: '#4287f5',
                    fillOpacity: 0.5,
                    radius: 1852 * harbour_radius
                }).addTo(map);
                // harbour marker
                var marker = L.marker([harbour.lat, harbour.lng]);
                marker.bindPopup("<h3>" + harbour.name + "</h3>dist: " + Number((harbour.dist).toFixed(1)) + " nm");
                marker.setIcon(stored_harbour_icon);
                marker.addTo(map);
                harbourMarkers.push(marker);
            });
            L.polyline(boat.trajectory.map(elt => {
                    return {
                        lat: parseFloat(elt.lat),
                        lng: parseFloat(elt.lng)
                    }
                }), {
                    color: 'red',
                    weight: 3,
                    opacity: 0.5,
                    smoothFactor: 1
                })
                .addTo(map);

            // set boat and harbours in bounds
            var group = new L.featureGroup(harbourMarkers.concat(boatMarker));
            map.fitBounds(group.getBounds());

            //slider setup
            $('input.refill-range').change(function(e) {
                $(this).next(".label").text($(this).val());
            });

            // refill button setup
            $("#refill-button").click(function() {
                if ($('input[name="food-amount"]').val() > 0) {
                    $.post("../../backend/rest/ask-for-refill.php", {
                        "app-token": getAppToken(),
                        "sw-boat-id": $('input[name="sw-boat-id"]').val(),
                        "food-amount": $('input[name="food-amount"]').val()
                    }, function(result, status) {
                        if (result.success) {
                            $('#food-stock').text(result.food_stock);
                            var capacity = parseInt($('#food-capacity').text());
                            $('input[name="food-amount"]').attr("max", "" + (capacity - result.food_stock));
                            $('input.refill-range').val(0);
                            $('input.refill-range').next(".label").text(0);
                        }
                        if (status == "success") {} else if (status == "timeout" || status == "error") {
                            console.log("error");
                        }
                    });
                }
            });

            //display other players setup
            $("#display-players-button").click(function() {
                if (!$(this).hasClass("disabled")) {
                    $("#display-players-button").addClass("disabled");
                    $.post("../../backend/rest/get-other-players.php", {
                        "app-token": getAppToken(),
                        "user-id": <?= $_SESSION["auth"]->id ?>
                    }, function(result, status) {
                        if (result.success) {
                            result.boats.forEach(boat => {
                                if (boat.trajectory.length > 0) {
                                    L.polyline(boat.trajectory.map(elt => {
                                            return {
                                                lat: parseFloat(elt.lat),
                                                lng: parseFloat(elt.lng)
                                            }
                                        }), {
                                            color: 'blue',
                                            weight: 3,
                                            opacity: 0.5,
                                            smoothFactor: 1
                                        })
                                        .addTo(map);
                                    L.marker([boat.trajectory[0].lat, boat.trajectory[0].lng])
                                        .setIcon(boat_icon)
                                        .bindPopup('<h5 class="text-primary">' + boat.name + '</h5><b>username:</b> ' + boat.username + '</br><b>type:</b> ' + boat.type)
                                        .addTo(map)
                                        .setRotationAngle(boat.angle)
                                        .setRotationOrigin("center");
                                }
                            });
                        } else {
                            console.error(result.message);
                            $("#display-players-button").removeClass("disabled");
                        }
                        if (status == "success") {} else if (status == "timeout" || status == "error") {
                            console.log("error");
                            $("#display-players-button").removeClass("disabled");
                        }
                    });
                }
            });

        }
    </script>
<?php } ?>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Dashboard <i><?= $db->getCVEBoatLabel($boat) ?></i></h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container content-wrapper">
    <div class="row">
        <?php if ($trajectory) { ?>
            <!-- MAP -->
            <div class="col-12 col-sm-9">
                <div id="map" style="height: 550px;"></div>
                <button id="display-players-button" type="button" class="btn btn-secondary">Display other players</button>
            </div>
            <div class="col-12 col-sm-3">
                <p>
                    Onboard food stock:
                    <?php
                    $food_capacity = $boat_charac->food_capacity;
                    echo '<span id="food-stock">' . $boat->food_stock . '</span> / <span id="food-capacity">' . $food_capacity . '</span>';
                    ?>
                </p>
                <p>
                    Nearest harbours:
                    <ul>
                        <?php
                        foreach ($nearestHarbours as $harbour) {
                            echo '<li>' . $harbour["harbour"]->name . ' (dist: ' . round($harbour["dist"], 1) . 'nm)</li>';
                        }
                        ?>
                    </ul>
                </p>
                <?php if ($inHarbour) { ?>
                    <div>
                        <p>
                            You are in <?= $nearestHarbour["harbour"]->name ?>
                        </p>
                        <p>
                            Refill list:
                        </p>
                        <ul>
                            <?php if ($boat->food_stock < $food_capacity) { ?>
                                <li>
                                    <label for="food-amount">Food</label>
                                    <input type="range" min="0" max="<?= ($food_capacity - $boat->food_stock) ?>" value="0" step="1" class="custom-range refill-range" name="food-amount">
                                    <span class="label">0</span>
                                </li>
                            <?php } ?>
                        </ul>
                        <button id="refill-button" type="button" class="btn btn-secondary">Refill</button>
                        <input type="hidden" name="sw-boat-id" value="<?= $boat->sw_boat_id ?>">
                    </div>
                <?php } ?>
            </div>
        <?php } else { ?>
            <p class="text-info">No information about this boat yet ! Please make sure you have sailed it for last 7 days.</p>
        <?php } ?>
    </div>
</div>
<?php require("../components/footer.php"); ?>