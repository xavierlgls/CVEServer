<?php

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<style>
    .caption {
        border: 2px solid red;
        color: red;
    }
</style>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Comment utiliser cet addon ?</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container content-wrapper">
    <p class="text-info">
        Cet addon est une plateforme conçue et développée pour la communauté et par la communauté. Il permet de rajouter un peu de réalisme à la planification des navigations dans le jeu <a href="https://sailaway.world">sailaway</a>. L'objectif est de faire parcourir un maximum de miles à son voilier sans manquer de ressources nécessaires à la vie de l'équipage et au bon fonctionnement du bateau.
    </p>
    <div class="container text-center">
        <div class="alert alert-warning">
            Il s'agit encore d'un prototype. Si vous rencontrez un bug ou si vous avez une question, merci de vous rendre sur le discord de l'addon
            <!-- TODO: lien -->
        </div>
    </div>
    <h2>Où commencer ?</h2>
    <p>
        Commencez par vous <a href="../register">créer un compte</a> sur ce site. Lors de sa création, vous le relierez à l'un de vos bateaux de sailaway. Choisissez-le bien !
    </p>
    <h2>Une fois connecté, comment jouer ?</h2>
    <p>
        Ce site est connecté à sailaway. Il permet de suivre la progression de votre bateau. Vous ne pouvez cependant le contrôler uniquement depuis le jeu. Vous devrez naviguer de port en port pour assurer le ravitaillement de votre embarcation. Expliquons dans un premier temps un peu comment utiliser l'interface:
    </p>
    <div class="row">
        <div class="col-md-2 text-center"></div>
        <div class="col-md-8 text-center">
            <img src="../../assets/tutorial_1.png" alt="" class="img-thumbnail">
        </div>
    </div>
    <p class="text-secondary  text-center">
        <i>Une fois votre connexion réussie, vous devriez obtenir une interface similaire à l'image présentée ci-dessus</i>
    </p>
    <div class="container text-center">
        <div class="alert alert-info">
            Si vous obtenez une page vierge sans carte, pas de panique ! Le site récupère périodiquement les données en jeu, il se peut qu'il n'ait pas encore eu le temps de trouver votre bateau ! (vous devez l'avoir utilisé durant les 7 derniers jours dans sailaway)
        </div>
    </div>
    <p>
        Les données affichées ne sont pas mises à jour en temps réel. Il vous faut donc refresh la page pour obtenir les dernières données. Mais pas besoin de le faire en permanence, le message en <span class="badge badge-warning caption">A</span> vous indique le temps restant avant la prochaine synchronisation entre ce site et sailaway. Il est donc inutile de mettre à jour la page tant qu'il reste du temps avant la prochaine synchronisation.
    </p>
    <h4>Gérer son bateau</h4>
    <ul>
        <li>
            L'onglet <span class="badge badge-warning caption">D</span>, celui sur lequel vous êtes par défaut lors de la connexion, vous permet de suivre votre bateau. Vous pouvez observer sa position et sa trajectoire sur la carte (ici à la position <span class="badge badge-warning caption">K</span>).
        </li>
        <li>
            Le panneau <span class="badge badge-warning caption">H</span> indique le stock restant de nourriture à bord. Il reste ici par exemple 9 jours de nourriture et votre bateau peut stocker au maximum 14 jours de vivre.
        </li>
        <li>
            Le panneau <span class="badge badge-warning caption">I</span> liste les ports les plus proches de votre bateau.
        </li>
        <li>
            La partie <span class="badge badge-warning caption">J</span> n'est visible que lorsque vous êtes à portée des pontons. C'est à dire lorsque vous rentrez dans la zone bleue (représentée en <span class="badge badge-warning caption">L</span>). Vous pouvez choisir la quantité de nourriture à embarquer.
        </li>
    </ul>
    <h4>Repérer les ports</h4>
    <p>
        Dans <a href="../harbours">l'onglet <span class="badge badge-warning caption">C</span></a>, vous trouverez une carte qui vous présente l'ensemble des ports disponibles dans le monde. Idéal pour planifier son voyage !
    </p>
    <h4>Trouver les meilleurs joueurs</h4>
    <p>
        L'onglet <span class="badge badge-warning caption">B</span> affiche les meilleurs joueurs, c'est à dire ceux qui ont cumulé le plus de miles sans jamais recontrer une pénurie. Vous y serez peut-être un jour, qui sait ?
    </p>
    <div class="container text-center">
        <div class="alert alert-danger">
            Si votre jauge de nourriture se retrouve à zéro ou si vous vous téléportez, votre cumul de miles est remis à zéro !
        </div>
    </div>
    <h4>Gérer son compte</h4>
    <p>
        Vous pourrez accéder à toutes les informations relatives à votre compte dans l'onglet <span class="badge badge-warning caption">E</span>. Vous pouvez également y modifier votre bateau.
    </p>
    <div class="container text-center">
        <div class="alert alert-info">
            Cette page d'aide est accessible à ton moment depuis l'onglet <span class="badge badge-warning caption">G</span>
        </div>
    </div>
</div>

<?php require("../components/footer.php"); ?>