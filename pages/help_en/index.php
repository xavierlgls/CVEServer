<?php

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<style>
    .caption {
        border: 2px solid red;
        color: red;
    }
</style>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>How to use this addon?</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container content-wrapper">
    <p class="text-info">
        This addon is a platform designed and developed by the community and for the community. It allows you to add a little realism to the planning of navigations in the <a href="https://sailaway.world">sailaway</a> game. The objective is to make sailboat sail a maximum of miles without lacking the resources necessary for the life of the crew and the proper functioning of the boat.
    </p>
    <div class="container text-center">
        <div class="alert alert-warning">
            It is still a prototype. If you encounter a bug or if you have a question, please go to the addon discord
            <!-- TODO: lien -->
        </div>
    </div>
    <h2>Where to start ?</h2>
    <p>
        Start by <a href="../register">creating an account</a> on this site. When it is created, you will connect it to one of your sailaway boats. Choose it well!
    </p>
    <h2>Once connected, how to play?</h2>
    <p>
        This site is connected to sailaway. It allows you to follow the progress of your boat. However, you cannot control it only from the game. You will have to navigate from port to port to ensure the supply of your boat. Let’s first explain how to use the interface:
    </p>
    <div class="row">
        <div class="col-md-2 text-center"></div>
        <div class="col-md-8 text-center">
            <img src="../../assets/tutorial_1.png" alt="" class="img-thumbnail">
        </div>
    </div>
    <p class="text-secondary  text-center">
        <i>Once your connection is successful, you should get an interface similar to the image shown above</i>
    </p>
    <div class="container text-center">
        <div class="alert alert-info">
            If you get a blank page without a card, don't panic! The site periodically retrieves the data in play, it may not have had time to find your boat yet! (you must have used it during the last 7 days in sailaway)
        </div>
    </div>
    <p>
        The data displayed is not updated in real time. So you need to refresh the page to get the latest data. But no need to do it constantly, the message in <span class="badge badge-warning caption">A</span> tells you the time remaining before the next synchronization between this site and sailaway. There is therefore no need to update the page as long as there is time before the next synchronization.
    </p>
    <h4>Manage your boat</h4>
    <ul>
        <li>
            The <span class="badge badge-warning caption">D</span> tab , the one on which you are by default when connecting, allows you to follow your boat. You can observe its position and trajectory on the map (here at position <span class="badge badge-warning caption">K</span> ).
        </li>
        <li>
            Panel <span class="badge badge-warning caption">D</span> indicates the remaining stock of food on board. Here, for example, 9 days of food remain and your boat can store a maximum of 14 days of food.
        </li>
        <li>
            Panel <span class="badge badge-warning caption">I</span> lists the ports closest to your boat.
        </li>
        <li>
            Part <span class="badge badge-warning caption">J</span> is only visible when you are within reach of the pontoons. That is to say when you enter the blue zone (represented in <span class="badge badge-warning caption">L</span> ). You can choose the amount of food to take on board.
        </li>
    </ul>
    <h4>Locate the ports</h4>
    <p>
        In the <a href="../harbours"><span class="badge badge-warning caption">C</span> tab</a>, you will find a map which presents all the ports available in the world. Ideal for planning your trip!
    </p>
    <h4>Find the best players</h4>
    <p>
        Tab <span class="badge badge-warning caption">B</span> displays the best players, i.e. those who have accumulated the most miles without ever encountering a shortage. One day you may be there, who knows?
    </p>
    <div class="container text-center">
        <div class="alert alert-danger">
            If your food gauge goes to zero or if you are teleporting, your mileage is reset to zero!
        </div>
    </div>
    <h4>Manage your account</h4>
    <p>
        You will be able to access all the information relating to your account in the <span class="badge badge-warning caption">E</span> tab. You can also modify your boat there.
    </p>
    <div class="container text-center">
        <div class="alert alert-info">
        This help page is accessible at your time from the <span class="badge badge-warning caption">G</span> tab
        </div>
    </div>
</div>

<?php require("../components/footer.php"); ?>