<?php
require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>My account</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container content-wrapper">
    <?php if (isset($_SESSION['auth'])) {
        $boat = $db->getCVEBoatFromCVEUserId($_SESSION["auth"]->id); ?>
        <table class="table">
            <tbody>
                <tr>
                    <td>
                        <strong>Sailaway username:</strong>
                    </td>
                    <td>
                        <?= $db->getSailawayUserName($_SESSION['auth']->sw_user_id) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Boat:</strong>
                    </td>
                    <td>
                        <?php
                        echo $db->getCVEBoatLabel($boat);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Score:</strong>
                    </td>
                    <td>
                        <?php
                        echo $boat->score.' nm';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Email:</strong>
                    </td>
                    <td>
                        <?= $_SESSION['auth']->email ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Permission:</strong>
                    </td>
                    <td>
                        <?php
                        switch ($_SESSION['auth']->permission) {
                            case 0:
                                echo 'player';
                                break;
                            case 1:
                                echo 'contributor';
                                break;
                            case 2:
                                echo 'administrator';
                                break;
                            default:
                                echo 'unknown';
                                break;
                        }
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="form-group">
            <form id="boat-change-form" action="../../backend/authentication.php" method="post">
                <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY; ?>">
                <input type="hidden" name="action" value="change-boat">
                <input type="hidden" name="user-id" value="<?= $_SESSION['auth']->id ?>">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">&#x26f5;</span>
                        </div>
                        <select id="boat-id" name="boat-id" class="form-control">
                            <?php
                            $currentBoatId = $db->getCVEBoatFromCVEUserId($_SESSION['auth']->id)->sw_boat_id;
                            $boats = $db->getSailawayBoatsFromSwUserId($_SESSION['auth']->sw_user_id);
                            foreach ($boats as $boat) {
                                if ($boat->id != $currentBoatId) {
                                    echo '<option value="' . $boat->id . '">' . $boat->name . ' (' . $boat->type . ')</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <p class="text-danger">Your score is going to be lost if you change your boat</p>
                <button type="submit" class="btn btn-primary">Change my boat</button>
            </form>
        </div>
        <hr>
        <div class="form-group">
            <form id="deletion-form" action="../../backend/authentication.php" method="post">
                <input type="hidden" name="app-token" value="<?= $CVE_WEB_API_KEY ?>">
                <input type="hidden" name="action" value="delete">
                <input type="hidden" name="id" value="<?= $_SESSION['auth']->id ?>">
                <button type="submit" class="btn btn-danger">Delete my account</button>
            </form>
        </div>
    <?php } ?>
</div>
<script>
    $("#deletion-form").submit(function(event) {
        if (!confirm('Are you sure to delete your account ?\nAll informations about your progression are going to be lost !')) {
            event.preventDefault();
        }
    });
    $("#boat-change-form").submit(function(event) {
        if (!confirm('Are you sure to change your boat ?\nAll informations about your progression are going to be lost !')) {
            event.preventDefault();
        }
    });
</script>
<?php require("../components/footer.php"); ?>