<?php

require("../components/header.php");
require("../components/navbar.php");
require("../components/flash.php");
?>
<div style="padding: 0.5em; background-color: #EEEEEE;">
    <h3>Logs</h3>
</div>
<hr class="text-warning" style="border: 2px solid; border-radius: 1px; margin-top: 0;">
<div class="container content-wrapper">
    <?php if (isset($_SESSION['auth'])) {
        if ($_SESSION['auth']->permission >= 2) {
            $logs = $db->getLogs();
    ?>
            <table class="table table-hover">
                <tr>
                    <th>
                        Date
                    </th>
                    <th>
                        Text
                    </th>
                </tr>
                <?php
                foreach ($logs as $log) {
                    echo '<tr><td>' . $log->date . '</td><td>' . $log->text . '</td></tr>';
                }
                ?>
            </table>
    <?php
        }
    } else {
        addFlashError('you dont have the permission to access this page');
        header('Location: ../home');
    } ?>
</div>

<?php require("../components/footer.php"); ?>