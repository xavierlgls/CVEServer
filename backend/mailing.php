<?php

function sendRegistrationConfirmationMail($email, $username, $token, $id)
{
    $subject = "CVE registration confirmation";
    $headers = 'From: CVE Server <u986909396@srv190.main-hosting.eu>' . PHP_EOL .
        'Content-type: text/html' . PHP_EOL .
        'X-Mailer: PHP/' . phpversion();
    $confirm_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?token=" . $token . "&user_id=" . $id . "&action=register-confirmation";
    $img_link = "http://$_SERVER[HTTP_HOST]/public/assets/confirm_icon.png";
    $message = '<html><body>Dear ' . $username . ',<br>Welcom to the complete voyaging experience ! Please click on the link bellow to confirm your account.<br><a href="' . $confirm_link . '"><img src="' . $img_link . '" alt="link"/></a></body></html>';

    mail($email, $subject, $message, $headers);
}

function sendPasswordResetRequestMail($email, $username, $token, $id)
{
    $subject = "CVE password reset";
    $headers = 'From: CVE Server <u986909396@srv190.main-hosting.eu>' . PHP_EOL .
        'Content-type: text/html' . PHP_EOL .
        'X-Mailer: PHP/' . phpversion();
    $reset_link = "http://$_SERVER[HTTP_HOST]/public/pages/password_change?token=" . $token . "&user_id=" . $id . "&username=" . $username . "&action=password-reset";
    $img_link = "http://$_SERVER[HTTP_HOST]/public/assets/pwd_change_icon.png";
    $message = '<html><body>Dear ' . $username . ',<br>Please click on the link bellow to set your new password.<br><a href="' . $reset_link . '"><img src="' . $img_link . '" alt="link"/></a></body></html>';

    mail($email, $subject, $message, $headers);
}
