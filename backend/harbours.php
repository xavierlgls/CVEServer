<?php
header('Content-Type: application/json');

require("../../config_v1.php");
require("db.php");
require("tools.php");
$db = new DB();

function updateFile($db)
{
    $list = $db->getAllHarbours();
    $header = array('Latitude', 'Longitude', 'Name');
    if ($f = @fopen('../downloads/harbours.csv', 'w')) {
        fputcsv($f, $header);
        foreach ($list as $row) {
            fputcsv($f, array($row->lat, $row->lng, $row->name));
        }
        fclose($f);
    }
}

$return;
$return["success"] = false;

if (!isRequestAuthenticated()) {
    $return["message"] = "You are not authorized to use this service. Please use a right key.";
} else {
    if (isset($_POST["request"])) {
        switch ($_POST["request"]) {
            case "add":
                $user_id = 1;
                if (isset($_POST["user-id"])) {
                    $user_id = intval($_POST["user-id"]);
                }
                if (isset($_POST["harbours"])) {
                    $harbours = json_decode($_POST["harbours"]);
                    $result = array(0);
                    foreach ($harbours as $harbour) {
                        $name = htmlspecialchars($harbour->name);
                        $id = $db->insertANewHarbour($name, floatval($harbour->lat), floatval($harbour->lng), $user_id);
                        array_push($result, $id);
                    }
                    updateFile($db);
                    $return["ids"] = $result;
                    $return["message"] = "Harbour(s) added";
                    $return["success"] = true;
                } else {
                    $return["message"] = "Wrong request structure";
                }
                break;
            case "get":
                // returns a json that contains the list of all registered harbours
                try {
                    $harbours = $db->getAllHarbours();
                    $return["harbours"] = $harbours;
                    $return["message"] = "Here are all harbours";
                    $return["success"] = true;
                } catch (Exception $e) {
                    $return["message"] = "Error while getting harbours in the database";
                }
                break;
            case "remove":
                $db->removeHarbours(json_decode($_POST["id-list"], true));
                updateFile($db);
                $return["message"] = "Harbour(s) removed";
                $return["success"] = true;
                break;
            default:
                $return["message"] = "Wrong request structure";
        }
    } else {
        $return["message"] = "Nothing happens...";
    }
}

echo json_encode($return);
