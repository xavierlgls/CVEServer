<?php
session_start();

require("../../config_v1.php");
require("db.php");
require("tools.php");
$db = new DB();
// Request authentication check
if (!isRequestAuthenticated()) {
    echo 'You are not authorized to use this service';
    return;
}

if (isset($_POST["action"]) and $_SESSION['auth']->permission >= 2) {
    switch ($_POST["action"]) {
        case "permission":
            handlePermission($db);
            break;
        case "deletion":
            handleDeletion($db);
            break;
        case "registration-confirmation":
            handeConfirmRegistration($db);
    }
} else {
    addFlashError('wrong request content');
    header('Location: ../pages/users_management');
}

function handlePermission($db)
{
    if (isset($_POST['id']) and isset($_POST['new-permission'])) {
        $db->setUserPermission($_POST['id'], $_POST['new-permission']);
    } else {
        addFlashError('wrong request content');
    }
    header('Location: ../pages/users_management');
}

function handleDeletion($db)
{
    if (isset($_POST['id'])) {
        $db->removeUser($_POST['id']);
    } else {
        addFlashError('wrong request content');
    }
    header('Location: ../pages/users_management');
}

function handeConfirmRegistration($db)
{
    if (isset($_POST['id'])) {
        $db->confirmRegistration($_POST['id']);
    } else {
        addFlashError('wrong request content');
    }
    header('Location: ../pages/users_management');
}
