<?php

class DB
{
    function __construct()
    {
        global $DB_NAME;
        global $DB_USERNAME;
        global $DB_HOST;
        global $DB_PASSWORD;
        $this->db = new PDO('mysql:dbname=' . $DB_NAME . ';host=' . $DB_HOST, $DB_USERNAME, $DB_PASSWORD);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    /* ============================== HARBOURS ============================== */

    public function harbourPickerLogin($username, $password)
    {
        //Returns true id the user is registered
        $req = $this->db->prepare("SELECT count(*) FROM harbour_pickers WHERE name=? AND password=?");
        $req->execute(array($username, $password));
        if ($req->fetchColumn() > 0) {
            $req = $this->db->prepare("SELECT * FROM harbour_pickers WHERE name=? AND password=?");
            $req->execute(array($username, $password));
            return $req->fetch();
        } else {
            return NULL;
        }
    }

    public function insertANewHarbour($name, $lat, $lng, $user_id)
    {
        $req = $this->db->query("INSERT INTO harbours (name, lat, lng, picker_id) VALUES (" . $this->db->quote($name) . ", $lat, $lng, $user_id)");
        return $this->db->lastInsertId();
    }

    public function removeHarbours($idList)
    {
        foreach ($idList as $id) {
            $this->db->query("DELETE FROM harbours WHERE harbour_id=" . $id);
        }
    }

    public function getAllHarbours()
    {
        return $this->db->query("SELECT * FROM harbours")->fetchAll();
    }

    public function getHarboursNumber()
    {
        $req = $this->db->query("SELECT count(*) FROM harbours");
        return $req->fetchColumn();
    }

    /**
     * Returns N nearest harbours
     */
    public function getNearestHarbours($lat, $lng, $N)
    {
        $distComp = function ($harbour1, $harbour2) use ($lat, $lng) {
            $dist1 = DB::distance($lat, $lng, $harbour1->lat, $harbour1->lng, "N");
            $dist2 = DB::distance($lat, $lng, $harbour2->lat, $harbour2->lng, "N");
            if ($dist1 == $dist2) {
                return 0;
            } else if ($dist1 < $dist2) {
                return -1;
            } else {
                return 1;
            }
        };

        $harbours = $this->getAllHarbours();
        usort($harbours, $distComp);

        $output = array();
        if ($N > sizeof($harbours)) {
            $N = sizeof($harbours);
        }
        for ($i = 0; $i < $N; $i++) {
            $output[$i]["dist"] = DB::distance($lat, $lng, $harbours[$i]->lat, $harbours[$i]->lng, "N");
            $output[$i]["harbour"] = $harbours[$i];
        }
        return $output;
    }

    public function getNearestHarbour($lat, $lng)
    {
        $container = NULL;
        $harbours = $this->getAllHarbours();
        foreach ($harbours as $harbour) {
            $dist = DB::distance($lat, $lng, $harbour->lat, $harbour->lng, "N");
            if ($container["dist"]) {
                if ($container["dist"] > $dist) {
                    $container["dist"] = $dist;
                    $container["harbour"] = $harbour;
                }
            } else {
                $container["dist"] = $dist;
                $container["harbour"] = $harbour;
            }
        }
        return $container;
    }

    /**
     * Km: unit = 'K'
     * Nautic miles: unit = 'N'
     * else miles
     */
    public static function distance($lat1, $lng1, $lat2, $lng2, $unit)
    {
        if (($lat1 == $lat2) && ($lng1 == $lng2)) {
            return 0;
        } else {
            $theta = $lng1 - $lng2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    /* ============================== CVE_USERS ============================== */

    /**
     * Returns the user if registered, else return NULL
     */
    public function checkLogin($email, $attemptPassword)
    {
        $req = $this->db->query('SELECT * FROM cve_users WHERE email=' . $this->db->quote($email));
        $user = $req->fetch();
        if ($user) {
            if (password_verify($attemptPassword, $user->password)) {
                return $user;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function getUserFromMail($email)
    {
        $req = $this->db->query('SELECT * FROM cve_users WHERE email=' . $this->db->quote($email));
        return $req->fetch();
    }

    public function getUserFromId($id)
    {
        $req = $this->db->query('SELECT * FROM cve_users WHERE id=' . $id);
        return $req->fetch();
    }

    public function getAllPlayers()
    {
        $req = $this->db->query('SELECT * FROM cve_users WHERE permission=0 ORDER BY confirmed_at DESC');
        return $req->fetchAll();
    }

    public function getAllContributors()
    {
        $req = $this->db->query('SELECT * FROM cve_users WHERE permission=1 ORDER BY confirmed_at DESC');
        return $req->fetchAll();
    }

    public function getAllAdministrators()
    {
        $req = $this->db->query('SELECT * FROM cve_users WHERE permission=2 ORDER BY confirmed_at DESC');
        return $req->fetchAll();
    }

    public function addUser($email, $passwordHashed, $sw_user_id, $confirmationToken)
    {
        $this->db->query('INSERT INTO cve_users (sw_user_id, password, email, confirmation_token) VALUES (' . $sw_user_id . ', ' . $this->db->quote($passwordHashed) . ', ' . $this->db->quote($email) . ', ' . $this->db->quote($confirmationToken) . ')');
        return $this->db->lastInsertId();
    }

    public function removeUser($id)
    {
        // remove from cve_users
        $this->db->query("DELETE FROM cve_users WHERE id=" . $id);
        // remove all boats linked to this user
        $this->db->query("DELETE FROM cve_boats WHERE cve_user_id=" . $id);
    }

    public function setUserPermission($id, $newPermission)
    {
        if ($newPermission <= 1) {
            $this->db->query('UPDATE cve_users SET permission=' . $newPermission . ' WHERE id=' . $id);
        }
    }

    public function setRememberToken($id, $token)
    {
        $this->db->query('UPDATE cve_users SET remember_token=' . $this->db->quote($token) . ' WHERE id=' . $id);
    }

    /**
     * Returns the user if the confirmation token is ok, else NULL
     */
    public function checkRegistrationConfirmationToken($user_id, $token)
    {
        $req = $this->db->query("SELECT * FROM cve_users WHERE id=" . $user_id);
        $user = $req->fetch();
        if ($token == NULL) {
            return -1;
        } else if ($token == $user->confirmation_token) {
            $this->db->query('UPDATE cve_users SET confirmation_token="", confirmed_at=NOW() WHERE id=' . $user_id);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns the user if the reset token is ok, else NULL
     */
    public function checkPasswordResetToken($token, $user_id, $newPassword)
    {
        $req = $this->db->query("SELECT * FROM cve_users WHERE id=" . $user_id);
        $user = $req->fetch();
        if ($token == $user->password_reset_token) {
            $hash = hashPassword($newPassword);
            $this->db->query('UPDATE cve_users SET password_reset_token=NULL, password="' . $hash . '" WHERE id=' . $user_id);
            return $user;
        } else {
            return NULL;
        }
    }

    public function setPasswordReset($user_id, $token)
    {
        $this->db->query('UPDATE cve_users SET password_reset_token=' . $this->db->quote($token) . ' WHERE id=' . $user_id);
    }

    public function confirmRegistration($user_id)
    {
        $this->db->query('UPDATE cve_users SET confirmation_token="", confirmed_at=NOW() WHERE id=' . $user_id);
    }

    /* ============================== CVE_BOATS ============================== */

    public function addCVEBoat($cve_user_id, $sw_boat_id, $boat_type)
    {
        $this->db->query('INSERT INTO cve_boats (cve_user_id, sw_boat_id, type) VALUES (' . $cve_user_id . ', ' . $sw_boat_id . ', ' . $boat_type . ')');
    }

    public function getAllCVEBoats()
    {
        $req = $this->db->query('SELECT * FROM cve_boats');
        return $req->fetchAll();
    }

    //TODO: remove (when multiple boats)
    public function getCVEBoatFromCVEUserId($cve_user_id)
    {
        $req = $this->db->query('SELECT * FROM cve_boats WHERE cve_user_id=' . $cve_user_id);
        return $req->fetch();
    }

    //TODO: remove (when multiple boats)
    public function changeUserBoat($cve_user_id, $new_sw_boat_id)
    {
        // remove all boats linked to this user
        $this->db->query("DELETE FROM cve_boats WHERE cve_user_id=" . $cve_user_id);
        // add a new boat
        $boat_type = $this->db->query('SELECT * FROM sailaway_boats WHERE id=' . $new_sw_boat_id)->fetch()->type;
        $this->addCVEBoat($cve_user_id, $new_sw_boat_id, $boat_type);
    }

    /**
     * Returns all boats attached to a cve_user
     */
    public function getCVEBoatsFromCVEUserId($cve_user_id)
    {
        $req = $this->db->query('SELECT * FROM cve_boats WHERE cve_user_id=' . $cve_user_id);
        return $req->fetchAll();
    }

    public function getCVEBoatsFromSWBoatId($sw_boat_id)
    {
        $req = $this->db->query('SELECT * FROM cve_boats WHERE sw_boat_id=' . $sw_boat_id);
        return $req->fetchAll();
    }

    public function getCVEBoatLabel($cve_boat)
    {
        $typeName = $this->db->query('SELECT name FROM sailaway_boat_characteristics WHERE type_id=' . $cve_boat->type)->fetch()->name;
        $boatName = $this->db->query('SELECT name FROM sailaway_boats WHERE id=' . $cve_boat->sw_boat_id)->fetch()->name;
        return $boatName . ' (' . $typeName . ')';
    }

    public function updateCVEBoatLocation($cve_boat_id, $newLat, $newLng, $newAngle)
    {
        $now = new DateTime();
        $newScore = 0;
        //teleportation check
        $previousPos = $this->db->query("SELECT * FROM cve_boat_positions WHERE cve_boat_id=$cve_boat_id ORDER BY DATE DESC")->fetch();
        if ($previousPos) {
            $distanceNm = DB::distance($previousPos->lat, $previousPos->lng, $newLat, $newLng, "N");
            $req = <<<SQL
            SELECT
                cve_boats.id AS id,
                sailaway_boats.name AS boat_name,
                sailaway_users.name AS username,
                cve_boats.score AS score,
                sailaway_boat_characteristics.max_speed AS max_speed,
                sailaway_boat_characteristics.name AS boat_type,
                cve_boats.food_stock AS food_stock
            FROM cve_boats
                INNER JOIN sailaway_boats ON sailaway_boats.id=cve_boats.sw_boat_id
                INNER JOIN cve_users ON cve_users.id=cve_boats.cve_user_id
                INNER JOIN sailaway_users ON cve_users.sw_user_id=sailaway_users.id
                INNER JOIN sailaway_boat_characteristics ON cve_boats.type=sailaway_boat_characteristics.type_id
            WHERE
                cve_boats.id=$cve_boat_id
            SQL;
            $boat  = $this->db->query($req)->fetch();
            $teleportation = 0;
            if ($boat) {
                $elapsedHours = (strtotime($now->format('Y-m-d H:i:s')) - strtotime($previousPos->date)) / 3600;
                $detectedSpeed = $distanceNm / $elapsedHours;
                if ($detectedSpeed > $boat->max_speed) {
                    // teleportation !
                    $teleportation = 1;
                    $this->storeLog('Teleportation detected for ' . $boat->username . ' using "' . $boat->boat_name . '" (' . $boat->boat_type . '), detected speed: ' .  round($detectedSpeed, 2) . ' knt, previous score: ' . $boat->score . ' nm');
                } else {
                    if ($boat->food_stock > 0) {
                        $newScore = $boat->score + $distanceNm;
                    } else {
                        // no food !
                    }
                }
            }
        }
        //update the score
        $this->db->query("UPDATE cve_boats SET score = " . round($newScore, 2) . " WHERE id=$cve_boat_id");
        //update the trajectory        
        $this->db->query("INSERT INTO cve_boat_positions (cve_boat_id, lat, lng, date, teleportation) VALUES ($cve_boat_id, $newLat, $newLng, " . $this->db->quote($now->format('Y-m-d H:i:s')) . ", $teleportation)");
        //update the angle
        $this->db->query("UPDATE cve_boats SET angle=" . $newAngle . " WHERE id=" . $cve_boat_id);
    }

    public function removeOldCVEBoatPositions()
    {
        global $TRAJECTORY_DURATION;
        $positions = $this->db->query("SELECT * FROM cve_boat_positions");
        foreach ($positions as $position) {
            $date = strtotime($position->date);
            $elapsedSec = intval(time() - $date);
            if ($elapsedSec > ($TRAJECTORY_DURATION * 3600)) {
                $this->db->query("DELETE FROM cve_boat_positions WHERE id=$position->id");
            }
        }
    }

    public function getTrajectory($cve_boat_id)
    {
        $traj = $this->db->query("SELECT * FROM cve_boat_positions WHERE cve_boat_id=$cve_boat_id ORDER BY date")->fetchAll();
        $output = [];
        $index = count($traj) - 1;
        while ($index >= 0) {
            array_push($output, $traj[$index]);
            if ($traj[$index]->teleportation) {
                break;
            } else {
                $index--;
            }
        }
        return $output;
    }

    public function refillBoat($sw_boat_id, $food_amount)
    {
        $boat = $this->db->query('SELECT * FROM cve_boats WHERE sw_boat_id=' . $sw_boat_id)->fetch();
        $foodCapacity = $this->db->query('SELECT * FROM sailaway_boat_characteristics WHERE type_id=' . $boat->type)->fetch()->food_capacity;
        $newFoodStock = $boat->food_stock + $food_amount;
        if ($newFoodStock > $foodCapacity) {
            $newFoodStock = $foodCapacity;
        }
        $this->db->query('UPDATE cve_boats SET food_stock=' . $newFoodStock . ' WHERE id=' . $boat->id);
        return $newFoodStock;
    }

    public function consumeFood($cve_boat_id)
    {
        $food_stock = $this->db->query('SELECT food_stock FROM cve_boats WHERE id=' . $cve_boat_id)->fetch()->food_stock;
        $food_stock--;
        if ($food_stock < 0) {
            $food_stock = 0;
        }
        $this->db->query('UPDATE cve_boats SET food_stock=' . $food_stock . ' WHERE id=' . $cve_boat_id);
    }

    public function getBestCVEBoats()
    {
        global $TOP_SCORES_SIZE;
        $req =  <<<SQL
        SELECT
            cve_boats.score as score,
            sailaway_users.name as username,
            sailaway_boats.name as boatname,
            sailaway_boat_characteristics.name as boattype
        FROM cve_boats
            INNER JOIN cve_users ON cve_users.id = cve_boats.cve_user_id
            INNER JOIN sailaway_users ON sailaway_users.id = cve_users.sw_user_id
            INNER JOIN sailaway_boats ON sailaway_boats.id = cve_boats.sw_boat_id
            INNER JOIN sailaway_boat_characteristics ON sailaway_boat_characteristics.type_id = cve_boats.type
        WHERE cve_users.confirmation_token=""
        ORDER BY cve_boats.score DESC
        LIMIT $TOP_SCORES_SIZE
        SQL;
        return $this->db->query($req)->fetchAll();
    }

    public function getCVEBoatRank($cve_boat_id)
    {
        $req = 'SELECT
                    rank
                FROM
                    (SELECT
                        id,
                        RANK() OVER(ORDER BY score DESC) AS rank
                    FROM cve_boats ) t
                WHERE
                    id=' . $cve_boat_id;
        return $this->db->query($req)->fetch()->rank;
    }

    /* ============================== SAILAWAY_BOATS ============================== */

    /**
     * Checks the boat is not already stored in the database, and add it
     */
    public function addSailawayBoat($id, $type, $user_id, $boat_name)
    {
        $req = $this->db->query('SELECT * FROM sailaway_boats WHERE id=' . $id);
        if ($req->fetchColumn() > 0) {
            // This sailaway-boat is already stored in the CVE database
            $this->db->query('UPDATE sailaway_boats SET name=' . $this->db->quote($boat_name) . ' WHERE id=' . $id);
            return;
        } else {
            $this->db->query('INSERT INTO sailaway_boats (id, user_id, type, name) VALUES (' . $id . ', ' . $user_id . ', ' . $type . ', ' . $this->db->quote($boat_name) . ')');
        }
    }

    /**
     * Returns the type of the boat defined by its sailaway id
     */
    public function getSailawayBoatType($sw_boat_id)
    {
        $req = $this->db->query('SELECT * FROM sailaway_boats WHERE id=' . $sw_boat_id);
        $boat = $req->fetch();
        return $boat->type;
    }

    public function getSailawayBoatsFromSwUserId($sw_user_id)
    {
        $req = <<<SQL
        SELECT
            sailaway_boats.id AS id,
            sailaway_boats.name AS name,
            sailaway_boat_characteristics.name AS type
        FROM `sailaway_boats`
            INNER JOIN `sailaway_boat_characteristics` ON sailaway_boats.type=sailaway_boat_characteristics.type_id
        WHERE sailaway_boats.user_id=$sw_user_id
        SQL;
        return $this->db->query($req)->fetchAll();
    }

    public function getSailawayBoatName($sw_boat_id)
    {
        return $this->db->query('SELECT name FROM sailaway_boats WHERE id=' . $sw_boat_id)->fetch()->name;
    }

    public function getBoatTypeName($type_id)
    {
        return $this->db->query('SELECT name FROM sailaway_boat_characteristics WHERE type_id=' . $type_id)->fetch()->name;
    }

    public function getUsername($sw_boat_id)
    {
        $req = <<<SQL
        SELECT sailaway_users.name AS username
        FROM sailaway_boats
            INNER JOIN sailaway_users ON sailaway_users.id = sailaway_boats.user_id
        WHERE sailaway_boats.id = $sw_boat_id
        SQL;
        return $this->db->query($req)->fetch()->username;
    }

    /* ============================== SAILAWAY DATA ============================== */

    /**
     * Checks the user is not already stored in the database, and add it
     */
    public function addSailawayUser($id, $name)
    {
        $req = $this->db->query('SELECT * FROM sailaway_users WHERE id=' . $id);
        if ($req->fetchColumn() > 0) {
            // This sailaway-user is already stored in the CVE database
            $this->db->query('UPDATE sailaway_users SET name=' . $this->db->quote($name) . ' WHERE id=' . $id);
            return;
        } else {
            $this->db->query('INSERT INTO sailaway_users (id, name) VALUES (' . $id . ', ' . $this->db->quote($name) . ')');
        }
    }

    /**
     * Returns true if this uername is used in sailaway
     */
    function sailawayUserExists($username)
    {
        $req = $this->db->query('SELECT * FROM sailaway_users WHERE name=' . $this->db->quote($username));
        return ($req->fetchColumn() > 0);
    }

    /**
     * Returns the sailaway username
     */
    function getSailawayUserName($user_id)
    {
        if (!isset($user_id)) {
            return "no sailaway username";
        }
        $req = $this->db->query('SELECT * FROM sailaway_users WHERE id=' . $user_id);
        if ($req->fetchColumn() > 0) {
            $req = $this->db->query('SELECT * FROM sailaway_users WHERE id=' . $user_id);
            $user = $req->fetch();
            return $user->name;
        } else {
            return '- undefined username -';
        }
    }

    /**
     * Returns the sailaway id
     */
    function getSailawayUserId($username)
    {
        $req = $this->db->query('SELECT id FROM sailaway_users WHERE name=' . $this->db->quote($username));
        $user = $req->fetch();
        return $user->id;
    }

    /**
     * Returns all boat characteristics
     */
    public function getSailawayBoatCharacteristics()
    {
        $req = $this->db->query('SELECT * FROM sailaway_boat_characteristics');
        return $req->fetchAll();
    }

    /**
     * Returns all characteristics of a type id
     */
    public function getCharacteristicsFromType($type)
    {
        $req = $this->db->query('SELECT * FROM sailaway_boat_characteristics WHERE type_id=' . $type);
        return $req->fetch();
    }

    /**
     * Sets a new food capacity for the defined boat
     */
    public function updateSailawayBoatFoodCapacity($type_id, $newCapacity)
    {
        $this->db->query('UPDATE sailaway_boat_characteristics SET food_capacity=' . $newCapacity . ' WHERE type_id=' . $type_id);
    }

    public function updateSailawayBoatMaxSpeed($type_id, $newSpeed)
    {
        $this->db->query('UPDATE sailaway_boat_characteristics SET max_speed=' . $newSpeed . ' WHERE type_id=' . $type_id);
    }

    public function getSailawayUserBoats($sw_user_id)
    {
        $query = <<<SQL
        SELECT sailaway_boats.name AS boat_name, sailaway_boat_characteristics.name AS type, sailaway_boats.id AS boat_id
        FROM `sailaway_boats`
            INNER JOIN `sailaway_users` ON sailaway_boats.user_id=sailaway_users.id
            INNER JOIN `sailaway_boat_characteristics` ON sailaway_boats.type=sailaway_boat_characteristics.type_id
        WHERE sailaway_users.id=$sw_user_id
        SQL;
        $req = $this->db->query($query);
        return $req->fetchAll();
    }


    /* ============================== SAILAWAY API ACCESS ============================== */

    /**
     * 
     */
    public function storeSWApiUseDate()
    {
        $this->db->query('INSERT INTO sailaway_api_uses () VALUES ()');
        //erase all dates older than 1 month
        $dates = $this->getSWApiUseDates();
        $storingInterval = 30; // days
        foreach ($dates as $date) {
            $now = time();
            $diff = $now - strtotime($date->date);
            if (round($diff / (60 * 60 * 24)) > $storingInterval) {
                $this->db->query('DELETE FROM sailaway_api_uses WHERE date=' . $this->db->quote($date->date));
            }
        }
    }

    /**
     * 
     */
    public function getSWApiUseDates()
    {
        $req = $this->db->query('SELECT * FROM sailaway_api_uses');
        return $req->fetchAll();
    }


    /* ============================== LOGS ============================== */

    public function storeLog($text)
    {
        $this->db->query('INSERT INTO logs (text) VALUES (' . $this->db->quote($text) . ')');
    }

    public function getLogs()
    {
        return $this->db->query('SELECT * FROM logs ORDER BY date DESC LIMIT 100')->fetchAll();
    }
}
