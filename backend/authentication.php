<?php
session_start();

require_once('../../config_v1.php');
require("db.php");
require("tools.php");
require("mailing.php");
$db = new DB();
// Request authentication check
function checkAppAuthentication()
{
    if (!isRequestAuthenticated()) {
        addFlashError('You are not authorized to use this service');
        return;
    }
}

if (isset($_POST["action"])) {
    switch ($_POST["action"]) {
        case "login":
            checkAppAuthentication();
            handleLogin($db);
            break;
        case "logout":
            checkAppAuthentication();
            handleLogout();
            break;
        case "register":
            checkAppAuthentication();
            handleRegister($db);
            break;
        case "unregister":
            checkAppAuthentication();
            handleUnregister($db);
            break;
        case "password-reset-request":
            checkAppAuthentication();
            handlePasswordResetRequest($db);
            break;
        case "delete":
            checkAppAuthentication();
            handleDeletionRequest($db);
            break;
        case "change-boat":
            checkAppAuthentication();
            handleChangeBoat($db);
            break;
        case "password-reset":
            //from mail
            handlePasswordReset($db);
            break;
    }
} else if (isset($_GET["action"])) {
    switch ($_GET["action"]) {
        case 'register-confirmation':
            //from mail
            handleRegisterConfirmation($db);
            break;
    }
}

function handleLogin($db)
{
    if (isset($_POST['email']) and isset($_POST['password'])) {
        $mail = htmlspecialchars($_POST['email']);
        $attemptPassword = htmlspecialchars($_POST['password']);
        $user = $db->checkLogin($mail, $attemptPassword);
        if ($user != NULL) {
            if ($user->confirmation_token == NULL) {
                $_SESSION['auth'] = $user;
                if ($_POST['remember']) {
                    if ($user->permission >= 1) {
                        addFlashError('because you have special permissions, your account is not remembered by the browser');
                    } else {
                        $remember_token = generateRandomStr(60);
                        $db->setRememberToken($user->id, $remember_token);
                        setcookie('remember', $user->id . '==' . $remember_token/* . sha1($user->id, $REMEMBER_SALT_KEY)*/, time() + 60 * 60 * 24 * 7, "/");
                    }
                }
                header('Location: ../pages/dashboard');
            } else {
                addFlashError('user account not confirmed yet');
                header('Location: ../pages/login');
            }
        } else {
            addFlashError('wrong email or password');
            header('Location: ../pages/login');
        }
    } else {
        addFlashError('request content error');
        header('Location: ../pages/login');
    }
}

function handleLogout()
{
    unset($_SESSION['auth']);
    setcookie('remember', NULL, -1, "/");
    $_SESSION['flash-success'] = 'successfully logout';
    header('Location: ../pages/home');
}

function handleRegister($db)
{
    if (isset($_POST['email-1']) and isset($_POST['password-1']) and isset($_POST['username']) and isset($_POST['boat-id'])) {
        if (!$db->sailawayUserExists($_POST['username'])) {
            addFlashError("This is not a sailaway username ! (" . $_POST['username'] . ")");
            header('Location: ../pages/register');
        }
        $email = htmlspecialchars($_POST['email-1']);
        $password = hashPassword(htmlspecialchars($_POST['password-1']));
        $username = htmlspecialchars($_POST['username']);
        if ($db->getUserFromMail($email) == NULL) {
            try {
                $confirmationToken = generateRandomStr(60);
                // add a new cve_user
                $sw_user_id = $db->getSailawayUserId(htmlspecialchars($_POST['username']));
                $cve_user_id = $db->addUser($email, $password, $sw_user_id, $confirmationToken);
                // add a new cve_boat
                $sw_boat_type = $db->getSailawayBoatType(intval($_POST['boat-id']));
                $sw_boat_id = intval($_POST['boat-id']);
                $db->addCVEBoat($cve_user_id, $sw_boat_id, $sw_boat_type);

                sendRegistrationConfirmationMail($email, $username, $confirmationToken, $cve_user_id);
                addFlashSuccess('a confirmation email has been sent to <u>' . $email . '</u> (have a look to your trashbox too)');
                header('Location: ../pages/home');
            } catch (Exception $e) {
                addFlashError(strval($e));
                header('Location: ../pages/register');
            }
        } else {
            addFlashError('this mail is already used');
            header('Location: ../pages/register');
        }
    } else {
        addFlashError('request content error');
        header('Location: ../pages/register');
    }
}

function handleUnregister($db)
{
    //TODO: check post content
    try {
        $db->removeUser($_SESSION['auth']->id);
        unset($_SESSION['auth']);
        header('Location: ../pages/home');
    } catch (Exception $e) {
        addFlashError(str($e));
    }
}

function handleRegisterConfirmation($db)
{
    if (isset($_GET['token']) and isset($_GET['user_id'])) {
        $status = $db->checkRegistrationConfirmationToken(intval($_GET['user_id']), htmlspecialchars($_GET['token']));
        if ($status == -1) {
            addFlashError("The registration of this user is already confirmed");
            header('Location: ../pages/home');
        } else if ($status == 0) {
            addFlashError("Something went wrong for the registration confirmation");
            header('Location: ../pages/home');
        } else if ($status == 1) {
            $user = $db->getUserFromId(intval($_GET['user_id']));
            $_SESSION['auth'] = $user;
            addFlashSuccess("Welcome " . $user->username . " !");
            header('Location: ../pages/home');
        }
        header('Location: ../pages/home');
    }
}

function handlePasswordResetRequest($db)
{
    if (isset($_POST['email'])) {
        $email = htmlspecialchars($_POST['email']);
        $user  = $db->getUserFromMail($email);
        if ($user != NULL) {
            $token = generateRandomStr(60);
            $db->setPasswordReset($user->id, $token);
            $username = $db->getSailawayUserName($user->sw_user_id);
            sendPasswordResetRequestMail($user->email, $username, $token, $user->id);
            addFlashSuccess('a mail has been sent to <u>' . $user->email . '</u> to reset your password (have a look to your trashbox too)');
            header('Location: ../pages/home');
        } else {
            addFlashError('unknown email');
            header('Location: ../pages/password_change_request');
        }
    } else {
        addFlashError('request content error');
        header('Location: ../pages/password_change_request');
    }
}

function handlePasswordReset($db)
{
    if (isset($_POST['token']) and isset($_POST['id']) and isset($_POST['password-1'])) {
        $token = htmlspecialchars($_POST['token']);
        $id = intval($_POST['id']);
        $newPassword = htmlspecialchars($_POST['password-1']);
        $user = $db->checkPasswordResetToken($token, $id, $newPassword);
        if ($user != NULL) {
            $_SESSION['auth'] = $user;
            addFlashSuccess('new password set');
            header('Location: ../pages/home');
        } else {
            addFlashError('token error');
            header('Location: ../pages/home');
        }
    } else {
        addFlashError('request content error');
        header('Location: ../pages/home');
    }
}

function handleDeletionRequest($db)
{
    if (isset($_POST['id'])) {
        $id = intval($_POST['id']);
        $db->removeUser($id);
        unset($_SESSION['auth']);
        setcookie('remember', NULL, -1, "/");
        addFlashSuccess('account deleted !');
        header('Location: ../pages/home');
    } else {
        addFlashError('something went wrong !');
        header('Location: ../pages/my_account');
    }
}

function handleChangeBoat($db)
{
    if (isset($_POST['boat-id']) && isset($_POST['user-id'])) {
        $boatId = intval($_POST['boat-id']);
        $userId = intval($_POST['user-id']);
        $db->changeUserBoat($userId, $boatId);
        addFlashSuccess('boat changed !');
    } else {
        addFlashError('something went wrong !');
    }
    header('Location: ../pages/my_account');
}
