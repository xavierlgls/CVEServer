<?php
header('Content-Type: application/json');

require("../../../config_v1.php");
require("../db.php");
require("../tools.php");
$db = new DB();

$return;
$return["success"] = false;

if (!isRequestAuthenticated()) {
    $return["message"] = "You are not authorized to use this service. Please use a right key.";
} else {
    if (isset($_POST["action"])) {
        switch ($_POST["action"]) {
            case "enable-service":
                enableService();
                break;
            case "disable-service":
                disableService();
                break;
            case "set-rate":
                setRate();
                break;
            case "get-rate":
                getRate();
                break;
        }
    }
}

echo json_encode($return);


function enableService()
{
    global $return;
    $json = json_decode(file_get_contents('../cron-jobs/cron-settings.json'));
    $json->enabled = true;
    file_put_contents('../cron-jobs/cron-settings.json', json_encode($json));
    $return["success"] = true;
    $return["message"] = "service enabled !";
}

function disableService()
{
    global $return;
    $json = json_decode(file_get_contents('../cron-jobs/cron-settings.json'));
    $json->enabled = false;
    file_put_contents('../cron-jobs/cron-settings.json', json_encode($json));
    $return["success"] = true;
    $return["message"] = "service disabled !";
}

function setRate()
{
    global $return;

    $now = time();
    $configJson = json_decode(file_get_contents('../cron-jobs/cron-settings.json'));
    $lastQueryDate = intval($configJson->last_query_date);
    $rate = intval($configJson->rate);
    $targetPeriodMin = intval((24 * 60) / $rate);
    $timeElapsedSinceLastQuery =  intval(($now - $lastQueryDate) / 60);

    $return["current"] = $timeElapsedSinceLastQuery;
    $return["target"] = $targetPeriodMin;

    if (isset($_POST["rate"])) {
        $json = json_decode(file_get_contents('../cron-jobs/cron-settings.json'));
        $json->rate = intval($_POST["rate"]);
        file_put_contents('../cron-jobs/cron-settings.json', json_encode($json));
        $return["success"] = true;
        $return["rate"] = $json->rate;
        $return["message"] = "rate set !";
    } else {
        $return["success"] = false;
        $return["message"] = "no defined rate in the request !";
    }
}

function getRate()
{
    global $return;
    $json = json_decode(file_get_contents('../cron-jobs/cron-settings.json'));
    $return["success"] = true;
    $return["rate"] = $json->rate;
}
