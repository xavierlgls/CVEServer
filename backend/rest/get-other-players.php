<?php
header('Content-Type: application/json');

require("../../../config_v1.php");
require("../db.php");
require("../tools.php");
$db = new DB();

$return;
$return["success"] = false;

if (!isRequestAuthenticated()) {
    $return["message"] = "You are not authorized to use this service. Please use a right key.";
} else {
    if (isset($_POST["user-id"])) {
        $user_id = intval($_POST["user-id"]);
        $boats = $db->getAllCVEBoats();
        $return["boats"] = [];
        foreach ($boats as $boat) {
            if ($boat->cve_user_id != $user_id) {
                $outputBoat;
                $outputBoat["trajectory"] = $db->getTrajectory($boat->id);
                $outputBoat["angle"] = intval($boat->angle);
                $outputBoat["name"] = $db->getSailawayBoatName($boat->sw_boat_id);
                $outputBoat["type"] = $db->getBoatTypeName($boat->type);
                $outputBoat["username"] = $db->getUsername($boat->sw_boat_id);
                array_push($return["boats"], $outputBoat);
            }
        }
        $return["success"] = true;
    } else {
        $return["message"] = "Please provide a user id";
    }
}

echo json_encode($return);
