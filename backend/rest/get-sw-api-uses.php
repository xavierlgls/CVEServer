<?php
header('Content-Type: application/json');

require("../../../config_v1.php");
require("../db.php");
require("../tools.php");
$db = new DB();

$return;
$return["success"] = false;

if (!isRequestAuthenticated()) {
    $return["message"] = "You are not authorized to use this service. Please use a right key.";
} else {
    $return["history"] = [];
    $dates = $db->getSWApiUseDates();

    foreach ($dates as $date) {
        $datetime = strtotime($date->date);
        $day = date('Y-m-d', $datetime);

        if (array_key_exists($day, $return["history"])) {
            $return["history"][$day]++;
        } else {
            $return["history"][$day] = 1;
        }
    }

    $return["success"] = true;
}

echo json_encode($return);
