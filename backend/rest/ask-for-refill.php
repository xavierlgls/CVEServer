<?php
header('Content-Type: application/json');

require("../../../config_v1.php");
require("../db.php");
require("../tools.php");
$db = new DB();

$return;
$return["success"] = false;

if (!isRequestAuthenticated()) {
    $return["message"] = "You are not authorized to use this service. Please use a right key.";
} else {
    if (isset($_POST["sw-boat-id"]) and isset($_POST["food-amount"])) {
        try {
            $boat_id = intval($_POST["sw-boat-id"]);
            $food_amount = intval($_POST["food-amount"]);
            $new_food_stock = $db->refillBoat($boat_id, $food_amount);
            $return["food_stock"] = $new_food_stock;
            $return["success"] = true;
        } catch (Exception $e) {
            $return["message"] = $e;
        }
    } else {
        $return["message"] = "no sw_boat id defined";
    }
}

echo json_encode($return);
