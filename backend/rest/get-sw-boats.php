<?php
header('Content-Type: application/json');

require("../../../config_v1.php");
require("../db.php");
require("../tools.php");
$db = new DB();

$return;
$return["success"] = false;

if (!isRequestAuthenticated()) {
    $return["message"] = "You are not authorized to use this service. Please use a right key.";
} else {
    if (isset($_POST["username"])) {
        $username = htmlspecialchars($_POST["username"]);
        if ($db->sailawayUserExists($username)) {
            $sw_id = $db->getSailawayUserId($username);
            $boats = $db->getSailawayUserBoats($sw_id);
            $return["success"] = true;
            $return["boats"] = $boats;
            $return["sw-user-id"] = $sw_id;
        } else {
            $return["message"] = "This is not a sailaway username";
        }
    } else {
        $return["message"] = "You must define a username !";
    }
}

echo json_encode($return);
