<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// job executed every minute
chdir(dirname(__FILE__));
require('../../../config_v1.php');
require('../db.php');
$db = new DB();

$now = time();
$configJson = json_decode(file_get_contents('cron-settings.json'));
$lastQueryDate = intval($configJson->last_query_date);
$rate = intval($configJson->rate);
$targetPeriodMin = intval((24 * 60) / $rate);
$timeElapsedSinceLastQueryMs =  intval(($now - $lastQueryDate) / 60);

// Detect if the file is executed from the admin panel (instead of a cron job)
$manualReqest = false;
if (isset($_SESSION['auth'])) {
    if ($_SESSION['auth']->permission  == 2) {
        $manualReqest = true;
    }
}


// Sailaway query
if (($timeElapsedSinceLastQueryMs >= $targetPeriodMin || $manualReqest) && $configJson->enabled) {
    //update the query time
    $configJson->last_query_date = $now;
    file_put_contents('cron-settings.json', json_encode($configJson));

    //request all datas to the SW api
    $url = 'https://backend.sailaway.world/cgi-bin/sailaway/TrackAllBoats.pl?key=' . $SW_API_KEY;
    $json = json_decode(file_get_contents($url));

    foreach ($json->boats as $boat) {
        // automatically add all new users and boats in the cve database
        if ($boat->ubtnr and $boat->ubtbtpnr and $boat->usrnr and $boat->ubtname) {
            $db->addSailawayBoat(intval($boat->ubtnr), intval($boat->ubtbtpnr), intval($boat->usrnr), htmlspecialchars($boat->ubtname));
        }
        if ($boat->usrnr and $boat->usrname) {
            $db->addSailawayUser(intval($boat->usrnr), htmlspecialchars($boat->usrname));
        }

        // update the location of all cve_boats
        $cve_boats = $db->getCVEBoatsFromSwBoatId(intval($boat->ubtnr));
        if (sizeof($cve_boats) > 0) // if this boat is tracked in the CVE server
        {
            foreach ($cve_boats as $cve_boat) {
                $db->updateCVEBoatLocation($cve_boat->id, $boat->ubtlat, $boat->ubtlon, $boat->ubtheading);
            }
        }
    }

    // remove all points in trajectories that are too old
    $db->removeOldCVEBoatPositions();
    $db->storeSWApiUseDate();
}

$configJson = json_decode(file_get_contents('cron-settings.json'));
$lastFoodUpdateDate = intval($configJson->last_food_update_date);
$food_update_rate = 1; // one per day
$targetPeriodFoodUpdateMin = intval((24 * 60) / $food_update_rate);
$timeElapsedSinceLastFoodUpdateMs =  intval(($now - $lastFoodUpdateDate) / 60);

//Food update
if ($timeElapsedSinceLastFoodUpdateMs >= $targetPeriodFoodUpdateMin) {
    $configJson->last_food_update_date = $now;
    file_put_contents('cron-settings.json', json_encode($configJson));
    // decrement by 1 all food stocks
    $cve_boats = $db->getAllCVEBoats();
    foreach ($cve_boats as $boat) {
        $db->consumeFood($boat->id);
    }
}

// go back to the admin panel
if ($manualReqest) {
    header("Location: ../../pages/sw_api_access_management");
}
