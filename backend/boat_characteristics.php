<?php
// header('Content-Type: application/json');

require("../../config_v1.php");
require("db.php");
require("tools.php");
$db = new DB();

// $return;
// $return["success"] = false;

if (!isRequestAuthenticated()) {
    // $return["message"] = "You are not authorized to use this service. Please use a right key.";
    addFlashError('You are not authorized to use this service. Please use a right key');
} else {
    $boat_types = $db->getSailawayBoatCharacteristics();
    foreach ($boat_types as $boat_type) {
        //food capacity
        if (isset($_POST['food_capacity_' . $boat_type->type_id])) {
            $db->updateSailawayBoatFoodCapacity($boat_type->type_id, $_POST['food_capacity_' . $boat_type->type_id]);
        }
        if (isset($_POST['max_speed_' . $boat_type->type_id])) {
            $db->updateSailawayBoatMaxSpeed($boat_type->type_id, $_POST['max_speed_' . $boat_type->type_id]);
        }
        //other characteristics...
    }
    // $return["success"] = true;
    // $return["message"] = "capacities updated !";
}

// echo json_encode($return);
header('Location:../pages/boat_characteristics_management');
